#pragma strict

public class Item extends MonoBehaviour
{
	// This is the item-type enum.
	enum ItemType
	{
		FEATHERS,
		HEALTH,
	}

	@SerializeField
	// This is the player, and an items reference.
	private var in_player : GameObject;
	static var items : Inventory; 
	@SerializeField
	// This is the collected item type.
	private var in_iType : ItemType;
	@SerializeField
	// This is the amount of the item collected.
	private var in_iAmount : int;

	function Start()
	{
		items = in_player.GetComponent.<Inventory>();
	}

	// When we enter the collider.
	function OnTriggerEnter(in_coll : Collider)
	{
		// ...and we are player...
		if(in_coll.gameObject == in_player)
		{
			if(items != null)
			{
				switch(in_iType)
				{
					case ItemType.FEATHERS:
						items.feathers += in_iAmount;
						break;
					case ItemType.HEALTH:
						items.health += in_iAmount;
						break;
				}
			}
			else Debug.Log("There's no items!");

			Destroy(gameObject);
		}
	}
}
