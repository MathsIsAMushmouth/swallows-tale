﻿using UnityEngine;
using System.Collections;

public class Damage : MonoBehaviour {

    public bool isDamaging;
    public float damage = 10;

    private void OnTriggerStay(Collider damageCol)
    {
        if(damageCol.tag == "Player")
        {
            damageCol.SendMessage((isDamaging)?"TakeDamage":"DontTakeDamage", Time.deltaTime * damage);
        }
    }
}
