﻿using UnityEngine;
using System.Collections;

public class playerControls : MonoBehaviour {

    public float speed = 10.0F;
    public float rotationSpeed = 100.0F;
    bool onGround = true;
    bool canDoubleJump = false;
    bool hasDoubleJumped = false;

    void Update()
    {
        //General moevement for the player
        float translation = Input.GetAxis("Vertical") * speed;
        float rotation = Input.GetAxis("Horizontal") * rotationSpeed;
        translation *= Time.deltaTime;
        rotation *= Time.deltaTime;
        transform.Translate(0, 0, translation);
        transform.Rotate(0, rotation, 0);
        

        //Raycast with recentering for the character
        RaycastHit playerBottom;
        Vector3 playerCentre = transform.position + GetComponent<CapsuleCollider>().center;

        //Test Raycast
        Debug.DrawRay(playerCentre, Vector3.down*1.1f, Color.cyan, 1);

        //Test for the character to be on the ground before jumping
        if (Physics.Raycast(playerCentre, Vector3.down, out playerBottom, 1.1f))
        {
            if (playerBottom.transform.gameObject.tag != "Player")
            {
                onGround = true;
            }
        }
        else
        {
            onGround = false;
        }
        //Feedback to test if true or false
        Debug.Log(onGround);

        //Gravity for Character, Possibly needed for glide
        //GetComponent<Rigidbody>().velocity -= Vector3.up * Time.deltaTime;

        //Allows the player to jump       
        if(Input.GetKeyDown("space") && onGround == true)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * 200);
            canDoubleJump = true;
        }
        //Allows player to double jump if it has jumped already
        else if(Input.GetKeyDown("space") && onGround == false && canDoubleJump == true)
        {
            GetComponent<Rigidbody>().AddForce(Vector3.up * 200);
            canDoubleJump = false;
            hasDoubleJumped = true;
        }

        //Allows the player to glide once it has double jumped    WIP
        if(Input.GetKey("space") && onGround == false && hasDoubleJumped == true)
        {
            //transform.Translate(Vector3.down * speed * 0.2f * Time.deltaTime);
            GetComponent<Rigidbody>().mass = 0.5f;
            Vector3.ProjectOnPlane(Vector3.down, Vector3.forward);
        }
        else
        {
            GetComponent<Rigidbody>().mass = 1;
            Vector3.ProjectOnPlane(Vector3.down, Vector3.down);
        }
    }    
}
