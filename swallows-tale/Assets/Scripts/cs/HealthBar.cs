﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour {

    public Image currentHealthBar;
    public Text healthPercentage;

    private float hitPoints = 100;
    private float maxHitPoints = 100;

    private void Start()
    {
        UpdateHealthBar();
    }

    private void UpdateHealthBar()
    {
        float healthRatio = hitPoints / maxHitPoints;
        currentHealthBar.rectTransform.localScale = new Vector3(healthRatio, 1, 1);
        healthPercentage.text = (healthRatio * 100).ToString("0") + "%";
    }

    private void TakeDamage(float damage)
    {
        hitPoints -= damage;
        Debug.Log("Taking Damage");

        if(hitPoints < 0)
        {
            hitPoints = 0;
            Debug.Log("You Are Dead");
        }
        UpdateHealthBar();
    }

    private void DontTakeDamage()
    {
        
    }
}
