﻿#pragma strict

public class Sounds extends MonoBehaviour
{
	@SerializeField
	private var s_jump : AudioClip;
	@SerializeField
	private var s_dJump : AudioClip;
	@SerializeField
	private var s_glide : AudioClip;

	private var e_state : PlayerController.State;
	private var source : AudioSource;

	function Start ()
	{
		e_state = gameObject.GetComponent.<PlayerController>().state;
		source = gameObject.GetComponent.<AudioSource>();
	}

	function Update ()
	{
		// If the state changed since the last frame...
		if(e_state != gameObject.GetComponent.<PlayerController>().state)
		{
			// reset our state...
			e_state = gameObject.GetComponent.<PlayerController>().state;
			switch(e_state)
			{
			// If we're jumping...
			case PlayerController.State.Jump:
				// Set our output sound to the jump clip...
				source.clip = s_jump;
				// Play our sound!
				source.Play();
				break;
			// If we're double jumping...
			case PlayerController.State.DJump:
				// Set our output sound to the jump clip...
				source.clip = s_dJump;
				// Play our sound!
				source.Play();
				break;
			// If we're jumping...
			case PlayerController.State.Glide:
				// Set our output sound to the jump clip...
				source.clip = s_glide;
				// Play our sound!
				source.Play();
				break;
			}
		}
		//  If the jump button isn't down, and the gliding sound
		// is playing, stop it prematurely.
		if(e_state == PlayerController.State.Glide && Input.GetAxis("Jump") == 0)
		{
			source.Stop();
		}
	}
}
