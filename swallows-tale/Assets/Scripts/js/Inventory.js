#pragma strict

public class Inventory extends MonoBehaviour
{
	public var feathers : int;
	public var health : int;
	public var hasKey : boolean;
	public var hasBrace : boolean;
}
