﻿#pragma strict

@SerializeField // Does the same thing it does in C#.
private var subject : UnityEngine.GameObject;
private var bar : UnityEngine.UI.Image;

function Start ()
{
	// Set the contents of healthBar to the image inside our subject.
	bar = subject.GetComponent(UnityEngine.UI.Image) as UnityEngine.UI.Image;
}

function Update () {
	// Code to change the amount the bar is filled goes here.
	// Example: 
	// bar.fillAmount -= 0.001;
	bar.fillAmount -= 0.001;
}