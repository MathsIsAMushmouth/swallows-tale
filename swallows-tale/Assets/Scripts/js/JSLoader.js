#pragma strict

import UnityEngine.SceneManagement;

function Start() {
	// Only specifying the sceneName or sceneBuildIndex will load
	// the scene with the Single mode
	SceneManager.LoadScene("OtherSceneName", LoadSceneMode.Additive);
}
