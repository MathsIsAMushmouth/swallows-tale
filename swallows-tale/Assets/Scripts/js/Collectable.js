#pragma strict

public class Collectable extends MonoBehaviour
{
	// This is the item-type enum.
	enum CollectableType
	{
		BRACE,
		KEY	
	}

	@SerializeField
	// This is the player, and an items reference.
	private var in_player : GameObject;
	static var items : Inventory; 
	@SerializeField
	// This is the collected item type.
	private var in_cType : CollectableType;
	@SerializeField
	// This is whether or not they have the item in question.
	private var in_cHas : boolean;

	function Start()
	{
		items = in_player.GetComponent.<Inventory>();
	}

	// When we enter the collider.
	function OnTriggerEnter(in_coll : Collider)
	{
		// ...and we are player...
		if(in_coll.gameObject == in_player)
		{
			if(items != null)
			{
				switch(in_cType)
				{
					case CollectableType.BRACE:
						items.hasBrace = in_cHas;
						break;
					case CollectableType.KEY:
						items.hasKey = in_cHas;
						break;
				}
			}
			else Debug.Log("There's no items!");

			Destroy(gameObject);
		}
	}
}
