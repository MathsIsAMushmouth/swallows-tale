﻿#pragma strict

private var MenuPanel : GameObject;
private var holdOpen : boolean;
private var cancelButt : float;

function Start () {
	MenuPanel = GameObject.Find("GameMenuPanel");
	MenuPanel.SetActive(false);

	holdOpen = false;
}

function Update () 
{
	cancelButt = Input.GetAxis("Cancel");
	
	if(holdOpen == false)
	{
		holdOpen = true;
		if(cancelButt > 0 && MenuPanel.activeSelf == false)
		{
			MenuPanel.SetActive(true);
		}
		else if(cancelButt > 0 && MenuPanel.activeSelf == true)
		{
			MenuPanel.SetActive(false);
		}
	}
	if(cancelButt <= 0) holdOpen = false;
}
