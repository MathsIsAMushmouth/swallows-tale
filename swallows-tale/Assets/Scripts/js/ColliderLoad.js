#pragma strict

public class ColliderLoad extends MonoBehaviour
{
	@SerializeField
	// This is the level to load onCollision.
	private var in_sceneIndex : int = 0;

	@SerializeField
	// This is the name of the player.
	private var in_player : GameObject;

	function Start()
	{

	}

	function OnTriggerEnter(in_coll : Collider)
	{
		Debug.Log("Player has entered collider!");
		if(in_coll.gameObject == in_player) 
		{
			Debug.Log("Player has left the level!");
			SceneManagement.SceneManager.LoadScene(in_sceneIndex);
		}
	}
}
