#pragma strict

public class CameraController extends MonoBehaviour
{
	@SerializeField
	private var player : Transform;

	function Start()
	{
	    transform.position = player.position + new Vector3(0, 5, -10);
	    transform.eulerAngles = new Vector3(30, 0, 0);
		Debug.Log(player.position.x + " " + player.position.y + " " + player.position.z);
		Debug.Log(transform.position.x + " " + transform.position.y + " " + transform.position.z);
	}
	
	function FixedUpdate()
	{
	    transform.RotateAround(player.position, Vector3.up, (Input.GetAxis("Mouse X") * 100) * Time.deltaTime);
		transform.eulerAngles.x -= Input.GetAxis("Mouse Y") * 100 * Time.deltaTime;
	}
}
