#pragma strict

public class PlayerController extends MonoBehaviour
{
	@SerializeField
	// This is how fast Teal will fall when gliding.
	private var wingEfficiency : float = 0.9;

	@SerializeField
	// This is... pretty self-explanatory, obviously.
	private var moveSpeed : float = 6.666;

	@SerializeField
	// This is the absolute lateral limit that Teal can move.
	private var maxMove : float = 20;

	@SerializeField
	// This is the camera transform. Used for rotating based on camera.
	private var cam : Transform;

	@SerializeField
	// This is the non-rotating base for our camera.
	private var platform : Transform;
	// Gravity, duh!
	private var gravity : float;
	// The direction to our camera.
	private var camVec3 : Vector3; 
	
	enum State
	{
		Standing,
		Jump,
		DJump,
		Glide
	}
	public var state : State = State.Jump;

	private var inventory : Inventory;

	function IsGrounded()
	{
		if(Physics.Raycast(transform.position, Vector3.down, 1.01)) return true;
		else return false;
	}

	private var sc : Collider;
	private var rb : Rigidbody;
	/*  TODO
	 *
	 *  - Develop a state machine.
	 *  - Accept input.
	 *  - Check state machine.
	 *  - Do thing based on input & state.
	 *
	 *  */
	
	function RotateByVelocity()
	{
		// Okay, I'll put a comment-per-line. This is kinda heady...
	    var direction : Vector2 = Vector2(rb.velocity.normalized.x, rb.velocity.normalized.z);
		// Calculate the angle, in radians, of the object rotation.
		var angle : float = Mathf.Atan(direction.x / direction.y);
		// Convert to degrees.
		angle = (angle / Mathf.PI) * 180;
		// Finally, rotate based on angle, but only if things haven't gone horribly wrong. 
		if(angle != null)
		{
			if(direction.Dot(direction, Vector2.up) < 0)
			{
				transform.eulerAngles = Vector3(0, angle - 180, 0);
				platform.transform.eulerAngles = Vector3(0, 0, 0);
			}
			else 	
			{
				transform.eulerAngles = Vector3(0, angle, 0);
				platform.transform.eulerAngles = Vector3(0, 0, 0);
			}
		}
		return;
	}

	// Initialise all our things
	function Start()
	{
		gravity = Physics.gravity.y;

		sc = gameObject.GetComponent(UnityEngine.Collider) as Collider;
		rb = gameObject.GetComponent(UnityEngine.Rigidbody) as Rigidbody;
		// Check if we have a Rigidbody.
		if(rb == null)
		{
			Debug.LogWarning("WARNING: You should add a Rigidbody to " + gameObject.name);
			rb = gameObject.AddComponent(UnityEngine.Rigidbody) as Rigidbody;
		}
		// Check if we have a Collider.
		if(sc == null)
		{
			Debug.LogWarning("WARNING: You should add a SphereCollider to " + gameObject.name);
			sc = gameObject.AddComponent(UnityEngine.SphereCollider) as SphereCollider;
		}

		inventory = gameObject.GetComponent.<Inventory>();

		Debug.Log(sc.bounds.min.x);
		Debug.Log(sc.bounds.min.y);
		Debug.Log(sc.bounds.min.z);

	}
	
	// This variable is used to keep the player from jumping multiple time with a single jump press.
	private var jumpHolder : boolean = false;
	private var delta : float;

	function Perpend(input : Vector3)
	{
		var output : Vector3 = input;
		output.x = input.z;
		output.z = -input.x;
		return output;
	}

	// These functions are used for camera-relative movement.
	function MoveAway(force : float)
	{
		rb.velocity += camVec3.normalized * moveSpeed * Time.deltaTime;
		return;
	}
	function MoveTowards(force : float)
	{
		rb.velocity += -camVec3.normalized * moveSpeed * Time.deltaTime;
		return;
	}
	function MoveLeft(force : float)
	{
		rb.velocity -= Perpend(camVec3.normalized) * moveSpeed * Time.deltaTime;
		return;
	}
	function MoveRight(force : float)
	{
		rb.velocity += Perpend(camVec3.normalized) * moveSpeed * Time.deltaTime;
		return;
	}

	function Update()
	{
		delta = Time.deltaTime;

		// Jump code.
		if(Input.GetButton("Jump") && jumpHolder == false)
		{
			jumpHolder = true;
			switch(state)
			{
				case State.Standing:
					Debug.Log("State: Jump.");
					state = State.Jump;
					rb.velocity.y = -gravity;
					break;
				case State.Jump:
					Debug.Log("State: Double Jump.");
					state = State.DJump;
					if(inventory.hasBrace) rb.velocity.y = -gravity;
					break;
				case State.DJump:
					Debug.Log("State: Gliding.");
					state = State.Glide;
					jumpHolder = false;
					break;
				case State.Glide:
					Debug.Log("State : Gliding.");
					// The IsGrounded() is neccessary to stop odd behaviour when gliding into the soil.
					if(!IsGrounded())
					{
						// Doing this allows us to hold the space bar.
						jumpHolder = false;
					}
					else jumpHolder = true;

					// Note: The faster we're falling, the more lift we get out of our gliding.
					rb.velocity.y += ((-gravity * wingEfficiency) * -rb.velocity.y) * delta;
					// Glide forwards when gliding.
					var direction : Vector3 = rb.velocity.normalized;
					direction.y = 0;
					rb.velocity = direction * 20;
					break;
			}
		}
		else if(!Input.GetButton("Jump")) jumpHolder = false;
		// Get the direction of the camera. This is needed for Teal's turn code.
		camVec3 = transform.position - cam.position;
		var zBuffer : float;
		// Remove the y axis. It's not needed, and rund the risk of screwing up our physics.
		camVec3.y = 0;
		// Move code. Turning Teal is handled below.
		// Moving forward.
		if(Input.GetAxis("Vertical") > 0) MoveAway(moveSpeed);
		// Moving backward.
		if(Input.GetAxis("Vertical") < 0) MoveTowards(moveSpeed);
		// Moving left.
		if(Input.GetAxis("Horizontal") < 0) MoveLeft(moveSpeed);
		// Moving left.
		if(Input.GetAxis("Horizontal") > 0)	MoveRight(moveSpeed);
		// Slow-down when not moving
		if(Input.GetAxis("Horizontal") == 0 && Input.GetAxis("Vertical") == 0)
		{
			rb.velocity = rb.velocity - Vector3(1, 1, 1) * Time.deltaTime;
		}

		// Rotate our character by their velocity.
		if(rb.velocity.x != 0 && rb.velocity.z != 0)
		{
			RotateByVelocity();
		}

		// Limit movement.
		if(rb.velocity.x > maxMove) rb.velocity.x = maxMove;
		if(rb.velocity.x < -maxMove) rb.velocity.x = -maxMove;
		if(rb.velocity.z > maxMove) rb.velocity.z = maxMove;
		if(rb.velocity.z < -maxMove) rb.velocity.z = -maxMove;

		// Check if we're grounded.
		if(rb.velocity.y <= 0 && IsGrounded()) state = State.Standing;
	}
}
