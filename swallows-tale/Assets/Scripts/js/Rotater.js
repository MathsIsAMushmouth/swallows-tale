#pragma strict

function Update() {
	// Rotate the object around its local X axis at 1 degree per second
	transform.Rotate(Vector3.up * 30 * Time.deltaTime);
	// ...also rotate around the World's Y axis
}
